@echo off

SETLOCAL

: instalation dependencies
set PROJ_HOME=c:\lab\proj\python-lab\face-recognition-v3

: setting basics
set THIS_BATCH_SHELL_HOME=%PROJ_HOME%\bin
IF [%1] EQU [%THIS_BATCH_SHELL_HOME%] (
    set CURRENT_DIR=%1
    set STAND_ALONE=FALSE
) ELSE (
    set CURRENT_DIR=%CD%
    set STAND_ALONE=TRUE
)
IF %CURRENT_DIR% NEQ %THIS_BATCH_SHELL_HOME% GOTO THIS_SHELL_LOCATION

: preparing variable
set PYTHON_UTIL_SCRIPTS_HOME=%PROJ_HOME%\util
set DATA_HOME=%PROJ_HOME%\backend\data
set PICKLE_PATH=%DATA_HOME%\pickle

: Parameters for pyhton shell
set PHOTOS_PATH=%DATA_HOME%\dataset
set PICLKE_FILENAME=%PICKLE_PATH%\encodings.pickle

: echo PROJ_HOME...............: %PROJ_HOME%
: echo THIS_BATCH_SHELL_HOME...: %THIS_BATCH_SHELL_HOME%
: echo CURRENT_DIR.............: %CURRENT_DIR%
: echo PYTHON_UTIL_SCRIPTS_HOME: %PYTHON_UTIL_SCRIPTS_HOME%
: echo DATA_HOME...............: %DATA_HOME%
: echo PHOTOS_PATH.............: %PHOTOS_PATH%
: echo PICLKE_FILENAME.........: %PICLKE_FILENAME%

IF NOT EXIST %PICLKE_FILENAME% ( 
    echo Pickle does not exist yet! 
    GOTO MAIN_SHELL   
) ELSE (
    GOTO PICKLE_ALREADY_EXIST 
) 

:MAIN_SHELL
echo It will be created. Please wait!
python %PYTHON_UTIL_SCRIPTS_HOME%\init-pickle.py --dataset %PHOTOS_PATH% --encodings %PICLKE_FILENAME% --detection-method cnn
GOTO END

:PICKLE_ALREADY_EXIST
echo Pickle file already exists at this location:
echo %PICLKE_FILENAME%
GOTO END

:THIS_SHELL_LOCATION
echo This shell is supposed to be in %THIS_BATCH_SHELL_HOME%
GOTO END

:END
IF %STAND_ALONE% EQU TRUE (
    cd %CURRENT_DIR%
) 
