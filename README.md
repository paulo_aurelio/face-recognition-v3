# Face Recognition
To run this application you need to have OpenCV installed. 

# OpenCV Requirements

### On Ubuntu or on other OS systems
Access the complete tutorial (for this POC we use the OpenCV 3 and Python 3):
- https://www.pyimagesearch.com/opencv-tutorials-resources-guides/


### On Windows 10 

**Installing Software Pre Requirements**
Please, install these prerequisites before continuing this guide.

##### Python 3.7.x 

This version already has **pip** bundled.

- https://www.python.org/downloads/windows/

We used: **python-3.7.3-amd64.exe**

***NOTE: choose your python installer in accordance with windows 10 platform, regarding to 32 or 64 bits***

##### Cmake
- https://cmake.org/download/

We used: **cmake-3.14.0-win64-x64.msi**

##### Microsoft Visual Studio 2017 BuildTools (Ferramentas de Compilação do Visual Studio 2017)
- https://visualstudio.microsoft.com/pt-br/thank-you-downloading-visual-studio/?sku=BuildTools&rel=15

We used: **vs_buildtools__97064796.1553610705.exe**

##### Python modules for OpenCV
```cmd
C:\Users\your-user> pip install face_recognition 
```
the followinging modules will be installed:
- Click-7.0 
- dlib-19.17.0 
- Pillow-5.4.1 
- face-recognition-1.2.3 
- face-recognition-models-0.3.0 
- numpy-1.16.2

##### Other Python modules required
```cmd
C:\Users\your-user> pip install imutils scipy unidecode websocket-client 
```
The following modules will be installed:
- imutils-0.5.2
- scipy-1.2.1
- unidecode-1.0.23
- websocket-client-0.56.0

# Downloading application from the git repository
After all the software for OpenCV has been installed, please
open a Windows Terminal (```cmd```) or (```powershell```) and run the following commands: 

```cmd
C:\Users\your-user> git clone http://NET002PRDLNX395.dcing.corp/claro-poc/face-recognition.git
C:\Users\your-user> cd face-recognition
C:\Users\your-user\face-recognition>
```


# Starting POC Applications
In order this procedure becomes easier, we created the **startup.bat** windows shell.
<p>This guide will cover the POC startup process by considering you will follow the instructions bellow through the use of this shell.</p>

Open a Windows Terminal (```cmd```) or (```powershell```) and run the following commands:
```cmd
C:\Users\your-user\face-recognition> startup.bat
 Usage: startup.bat [-all] | [-bf] [-rg | -r | -g]
 where:
      -all : All system will be starting up.
      -bf  : It starts backend and frontend applications
      -rg  : It starts both face recognition applications: recording and greeting
      -r   : It starts the face detection and recording application on WebCam Video Stream
      -g   : It starts the face recognition with greeting applications on USBCam Video Stream
 done!
```

### Only One Step to Start All System (Backend, Frontend, ReactJs Applications and Python Applications)
```cmd
C:\Users\your-user\face-recognition> startup.bat -all
```
***NOTE: The first time this shell runs on folder's project, all necessary dependencies for NodeJs will be installed!***

After the ```startup.bat``` had finished, your window's shell ***will be released!***

You are Finished!

### Or if you want to start system applications by running its modules independently

#### #1 - Start BackEnd Servers and FrontEnd ReactJs Applications together
```cmd
C:\Users\your-user\face-recognition> startup.bat -bf
```
***NOTE: The first time this shell runs on folder's project, all necessary dependencies for NodeJs will be installed!***

After the ```startup.bat``` had finished, your window's shell ***will be released!***

#### Or

#### #1.1 - Start the Backend first
If will desired run just only backend application, follow the instructions below:
```cmd
C:\Users\your-user\face-recognition> cd backend
C:\Users\your-user\face-recognition\backend> startup-backend.bat
```

Running in this way, after ```startup-backend.bat``` had finished, your window's shell ***will be locked!***

#### #1.2 - Start Frontend after Backend is running 

**Warning: We recommend you to start the frontend applications just only when the backend ones are running!**

Now that backend is running, follow instructions below:

Open a new Windows Terminal (```cmd```) or (```powershell```) and run the following commands:
```cmd
C:\Users\your-user\face-recognition> cd frontend
C:\Users\your-user\face-recognition\frontend> startup-frontend.bat 
```

Running in this way, after ```startup-frontend.bat``` had finished, your window's shell ***will be locked!***

#### #2 - Start Python Recording and Greeting Applications together

***NOTE: Before Start these procedures, verify the availability of the WebCam and USBCam on the local computer. The startup process bellow only works if the cameras are ready to be used!***

Open a new Windows Terminal (```cmd```) or (```powershell```) and run the following command:
```cmd
C:\Users\your-user\face-recognition> startup.bat -rg
```

#### Or

#### #2.1 - Start Pyhton Recording Only

Open a new Windows Terminal (```cmd```) or (```powershell```) and run the following command:
```cmd
C:\Users\your-user\face-recognition> startup.bat -r
```

#### #2.2 - Start Pyhton Greeting Only

Open a new Windows Terminal (```cmd```) or (```powershell```) and run the following command:
```cmd
C:\Users\your-user\face-recognition> startup.bat -g
```

