@echo off

SETLOCAL 
: instalation dependencies
set PROJ_HOME=c:\lab\proj\python-lab\face-recognition-v3

: setting basics
set THIS_BATCH_SHELL_HOME=%PROJ_HOME%
set CURRENT_DIR=%CD%
IF %CURRENT_DIR% NEQ %THIS_BATCH_SHELL_HOME% GOTO THIS_SHELL_LOCATION
cd %THIS_BATCH_SHELL_HOME%

: preparing variable
set PROJ_BATCH_SHELL_SCRIPTS_HOME=%PROJ_HOME%\bin
set BACKEND_HOME=%PROJ_HOME%\backend
set FRONTEND_HOME=%PROJ_HOME%\frontend
set RECORDING_BATCH_SHELL_SCRIPTS_HOME=%PROJ_HOME%\backend\apps\recording-faces\bin
set GREETING_BATCH_SHELL_SCRIPTS_HOME=%PROJ_HOME%\backend\apps\greeting-faces\bin

: echo PROJ_HOME..........................: %PROJ_HOME%
: echo THIS_BATCH_SHELL_HOME..............: %THIS_BATCH_SHELL_HOME%
: echo CURRENT_DIR........................: %CURRENT_DIR%
: echo PROJ_BATCH_SHELL_SCRIPTS_HOME......: %PROJ_BATCH_SHELL_SCRIPTS_HOME%
: echo BACKEND_HOME.......................: %BACKEND_HOME%
: echo FRONTEND_HOME......................: %FRONTEND_HOME%
: echo GREETING_BATCH_SHELL_SCRIPTS_HOME..: %GREETING_BATCH_SHELL_SCRIPTS_HOME%
: echo RECORDING_BATCH_SHELL_SCRIPTS_HOME.: %RECORDING_BATCH_SHELL_SCRIPTS_HOME%
GOTO MAIN_SHELL

:STARTUP_BACKEND_AND_FRONTEND
echo Checking Data preconditions...
call %PROJ_BATCH_SHELL_SCRIPTS_HOME%\create-new-pickle.bat %PROJ_BATCH_SHELL_SCRIPTS_HOME%  
echo.
echo Call Backend Startup Process
call %BACKEND_HOME%\startup-backend.bat  %BACKEND_HOME%
echo.
echo Call Frontend Startup Process
call %FRONTEND_HOME%\startup-frontend.bat %FRONTEND_HOME%
echo.
exit /b

:STARTUP_PYTHON_RECORDING_AND_GREETING_APPS
echo Starting Python Applications on both cameras
call :STARTUP_PYTHON_RECORDING_APP 
call :STARTUP_PYTHON_GREETING_APP
exit /b

:STARTUP_PYTHON_RECORDING_APP
echo Starting Python on WebCam 
call %RECORDING_BATCH_SHELL_SCRIPTS_HOME%\recording-faces.bat %RECORDING_BATCH_SHELL_SCRIPTS_HOME%
exit /b

:STARTUP_PYTHON_GREETING_APP
echo Starting Python Application on USBCam 
call %GREETING_BATCH_SHELL_SCRIPTS_HOME%\greeting-faces.bat %GREETING_BATCH_SHELL_SCRIPTS_HOME%
exit /b

:STARTUP_ALL
call :STARTUP_BACKEND_AND_FRONTEND
call :STARTUP_PYTHON_RECORDING_AND_GREETING_APPS
exit /b


:MAIN_SHELL
IF [%1] EQU [] (
    GOTO HELP
) ELSE (
    IF [%1] EQU [-all] (
        GOTO STARTUP_ALL
    ) 
    IF [%1] EQU [-bf] (
        GOTO STARTUP_BACKEND_AND_FRONTEND
    )
    IF [%1] EQU [-rg] (

        GOTO STARTUP_PYTHON_RECORDING_AND_GREETING_APPS
    ) 
    IF [%1] EQU [-r] (
        GOTO STARTUP_PYTHON_RECORDING_APP
    )
    IF [%1] EQU [-g] (
        GOTO STARTUP_PYTHON_GREETING_APP
    )
)
cd %THIS_BATCH_SHELL_HOME%
echo done!
GOTO :EOF

:HELP
echo "Usage: %0 [-all] | [-bf] [-rg | -r | -g]"
echo  where: 
echo       -all : All system will be starting up.
echo       -bf  : It starts backend and frontend applications
echo       -rg  : It starts both face recognition applications: recording and greeting
echo       -r   : It starts the face detection and recording application on WebCam Video Stream
echo       -g   : It starts the face recognition with greeting applications on USBCam Video Stream
GOTO :EOF

:THIS_SHELL_LOCATION
echo This shell is supposed to be in %THIS_BATCH_SHELL_HOME%
echo Fixing Shell's PROJ_HOME variable, wait!

set FIXED_PROJ_HOME=%CD%
set PYTHON_UTIL_SCRIPTS_HOME=%FIXED_PROJ_HOME%\util
IF NOT EXIST %PYTHON_UTIL_SCRIPTS_HOME%\replace-str-in-file.py GOTO END

echo PROJ_HOME................: %PROJ_HOME%
echo FIXED_PROJ_HOME..........: %FIXED_PROJ_HOME%

: Fixing create-new-pickle.bat
set BATCH_FILENAME=%FIXED_PROJ_HOME%\bin\create-new-pickle.bat
echo Fixing PROJ_HOME in %BATCH_FILENAME%
python %PYTHON_UTIL_SCRIPTS_HOME%\replace-str-in-file.py --text-to-search %PROJ_HOME% --replacement-text %FIXED_PROJ_HOME% --text-file %BATCH_FILENAME%

: Fixing startup-backend.bat
set BATCH_FILENAME=%FIXED_PROJ_HOME%\backend\startup-backend.bat
echo Fixing PROJ_HOME in %BATCH_FILENAME%
python %PYTHON_UTIL_SCRIPTS_HOME%\replace-str-in-file.py --text-to-search %PROJ_HOME% --replacement-text %FIXED_PROJ_HOME% --text-file %BATCH_FILENAME%

: Fixing startup-frontend.bat
set BATCH_FILENAME=%FIXED_PROJ_HOME%\frontend\startup-frontend.bat
echo Fixing PROJ_HOME in %BATCH_FILENAME%
python %PYTHON_UTIL_SCRIPTS_HOME%\replace-str-in-file.py --text-to-search %PROJ_HOME% --replacement-text %FIXED_PROJ_HOME% --text-file %BATCH_FILENAME%

: Fixing recording-faces.bat
set BATCH_FILENAME=%FIXED_PROJ_HOME%\backend\apps\recording-faces\bin\recording-faces.bat
echo Fixing PROJ_HOME in %BATCH_FILENAME%
python %PYTHON_UTIL_SCRIPTS_HOME%\replace-str-in-file.py --text-to-search %PROJ_HOME% --replacement-text %FIXED_PROJ_HOME% --text-file %BATCH_FILENAME%

: Fixing greeting-faces.bat
set BATCH_FILENAME=%FIXED_PROJ_HOME%\backend\apps\greeting-faces\bin\greeting-faces.bat
echo Fixing PROJ_HOME in %BATCH_FILENAME%
python %PYTHON_UTIL_SCRIPTS_HOME%\replace-str-in-file.py --text-to-search %PROJ_HOME% --replacement-text %FIXED_PROJ_HOME% --text-file %BATCH_FILENAME%

: Fixing startup.bat
set BATCH_FILENAME=%FIXED_PROJ_HOME%\startup.bat
echo Fixing PROJ_HOME in %BATCH_FILENAME%
python %PYTHON_UTIL_SCRIPTS_HOME%\replace-str-in-file.py --text-to-search %PROJ_HOME% --replacement-text %FIXED_PROJ_HOME% --text-file %BATCH_FILENAME%

echo Fixed. 
call :HELP
GOTO :EOF
