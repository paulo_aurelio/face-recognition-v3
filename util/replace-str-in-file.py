#!/usr/bin/env python
# coding: utf-8

import fileinput
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--text-to-search", required=True, type=str, help="string to search in file")
ap.add_argument("-r", "--replacement-text", required=True, type=str, help="string to replace inside the file")
ap.add_argument("-f", "--text-file", required=True, type=str, help="path to find text-file")
args = vars(ap.parse_args())

text_to_search = args["text_to_search"]
replacement_text = args["replacement_text"]
filename = args["text_file"]

# fileinput already supports inplace editing. 
# It redirects stdout to the file in this case:
with fileinput.FileInput(filename, inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace(text_to_search, replacement_text), end='')    
    # end for
#end while
