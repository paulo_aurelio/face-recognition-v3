import React, { Component } from 'react';

import { 
  Button,
  Row,
  Card,
  CardBody
} from 'reactstrap';

import {  
  mockPyEmitPersonNameSentEvent, 
  subscribeToExpressOnline,
  subscribeToDetectingStarted, 
  subscribeToPersonHashSent,
  subscribeToConfirmPersonRecording,
  subscribeToRegisterImageStartedSent,
  emitClientStartedEvent,
  emitConfirmPersonRecordingEvent,
  emitRegisterImageStartedEvent,
  emitRegisterImageNotStartedEvent,
  emitRegisterImageFinishedEvent,
  emitRegisterImageCanceled,
  subscribeToRegisterImageSuccessfulEvent
 } from './clientSocketIOApi';
// 
// React Compoments
//
import ShowServerState from './components/ShowServerState/ShowServerState' 
//
//App Components
import ConfirmRegistration from './components/ConfirmRegistration/ConfirmRegistration'
import DisplayInvitationToRegister from './components/DisplayInvitationToRegister/DisplayInvitationToRegister'
import DisplayRegisterSuccess from './components/DisplayRegisterSuccess/DisplayRegisterSuccess'
import PersonNameForm from './components/PersonNameForm/PersonNameForm'

import './App.css';
 
// CLASS COMPONENTS
class App extends Component {

  constructor(props) {
    super(props);
 
    //INITIALIZATION FASE
    subscribeToExpressOnline( bol => this.setState({
        serverExpressOnLine: bol
    }));
    
    subscribeToDetectingStarted(obj => this.setState({ 
       //Initial App State
        operationMode: obj.message,
        personHash:'',
        personName:''
    }));

    //DETECTING PERSON IMAGE FASE
    subscribeToPersonHashSent( obj => {
        if(this.state.personHash === '') {
            this.setState({
              operationMode: obj.message,
              personHash: obj.personHash
            }, () => 
            setTimeout(() => { 
                          if( this.state.operationMode === 'Person Hash Sent'){
                            emitClientStartedEvent()
                          } 
            },15000))
        } 
    });

    //
    subscribeToConfirmPersonRecording((obj) => {
      this.setState({
        operationMode: obj.message
      });
    
    });

    //REGISTER PERSON IMAGE FASE
    subscribeToRegisterImageStartedSent(obj => this.setState({
        operationMode: obj.message
    }));

    //REGISTER PERSON IMAGE FASE FINISHED WITH SUCCESS
    subscribeToRegisterImageSuccessfulEvent(obj => 
      this.setState({
          operationMode: obj.message
      }, () => setTimeout(() => { 
        if( this.state.operationMode === 'Register Person Image Successful'){
            emitClientStartedEvent()
        } 
      },5000))
    );

  }
  // App State Object
  state = { // Initial State
    operationMode: 'Initialised: Waiting for Detecting Person Image to Start',
    testerPyButtonsVisible: false, 
    serverExpressOnLine : false,
    disabledFormButtons: false,
    personHash: '',
    personName: ''
  };
  //
  /*
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  */
  //
  //
  componentDidMount() { 
    //
    /*
    this.timerID = setInterval(
      () => { 
         // Initial screen!
         if( this.state.operationMode === 'Person Hash Sent' || 
             this.state.operationMode === 'Register Person Image Successful') {
            // Initialization Fase Finished, sending client started event!
            emitClientStartedEvent();
         }        
      },
      15000 // 20 secods in first screen idle!!!
    );
    */
    // Initialization Fase Finished, sending client started event!
    emitClientStartedEvent();
  }
  //
  //
  clickYesHandle = () => {
    //
    emitRegisterImageStartedEvent({
      personHash: this.state.personHash,
      personName: ''
    });    
  }
  //
  clickNotStartHandle = () => {
    //
    emitRegisterImageNotStartedEvent({
      personHash: this.state.personHash,
      personName: ''
    });
  }
  //
  clickCancelHandle = () => {
    //
    emitRegisterImageCanceled({
      personHash: this.state.personHash,
      personName: ''
    });
  }
  //
  clickHandlerShowConfirmRecording = () => {
    
    emitConfirmPersonRecordingEvent({
      personHash: this.state.personHash,
      personName: ''
    });

  }
  // Send Name from inputBox, if it was digited 
  handleSubmit =  e => {
    e.preventDefault();
    // send name and hash on [RegisterImageFinishedEvent]
    emitRegisterImageFinishedEvent ({
      personName: this.state.personName,
      personHash: this.state.personHash

    });
    //
    console.log('[App.js] - Sending [RegisterImageFinished] Event with data:  personName= ' + 
        this.state.personName  + ' personHash= ' +
        this.state.personHash);
  };  
  //
  // render React Compoments
  //
  render() {
    //
    // Main component redering
    //
    return (
      <div className="App fixed-bottom">
        <Row>
            <ShowServerState state={this.state.serverExpressOnLine} />      
        </Row>
        <Card className="Card-App justify-content-center align-items-center">
          <CardBody>
            { 
              // 'Initialised: Waiting for Detecting Person Image to Start' and 
              // 'Detecting Person Image Started'
              (this.state.operationMode === 'Person Hash Sent') &&                   
                  <DisplayInvitationToRegister 
                      speech={true} 
                      confirmationHandler={() => this.clickHandlerShowConfirmRecording()}                      
                   />                  
            }
            {
              // Confirm Person Recording
              (this.state.operationMode === 'Confirm Person Recording') && 
                  <ConfirmRegistration 
                      speech={true} 
                      clickYes={this.clickYesHandle} 
                      clickNo={this.clickNotStartHandle} 
                   />
            }
            {
              (this.state.operationMode === 'Register Person Image Started') &&
                  <PersonNameForm  
                      // Speech must works only if name doesn't have been provided and 
                      // if buttons are not disable!!! or in other words, just once this screen is rendering!
                      speech={this.state.personName === '' && !this.state.disabledFormButtons} 
                      //
                      submit={this.handleSubmit} 
                      value={this.state.personName}
                      changed={e => this.setState({ personName: e.target.value })}       
                      clickNo={this.clickCancelHandle}
                      buttonDisabled={this.state.disabledFormButtons}
                      microphoneClicked={(name, micOn) => this.setState({
                          personName: name,
                          disabledFormButtons: !micOn
                      })}  
                     />
            }
            {
                (this.state.operationMode === 'Register Person Image Successful') &&
                <DisplayRegisterSuccess 
                    speech={true}
                />
            }
          </CardBody>
        </Card>
        <p className="text-info">{this.state.operationMode} </p>               
        {this.state.testerPyButtonsVisible && <Button size='sm' onClick={() => mockPyEmitPersonNameSentEvent("927498237")} >Send Hash</Button> }                 
      </div>
    );
  }
}
//
export default App;
