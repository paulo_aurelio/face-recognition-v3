import openSocket from 'socket.io-client';


const reactSocket = openSocket('http://localhost:5000/face-recog-react-cli');


// INITIALIZATOIN FASE
const subscribeToExpressOnline = cb => {
    console.log('[clientSocketIOApi.js] - Subscribed to the [expressOnline] Event!');
    reactSocket.on('expressOnline', (bol) => cb(bol));
}

const emitClientStartedEvent = () => {
    console.log('[clientSocketIOApi.js] - Sending the [client-started] Event!');
    reactSocket.emit('client-started', {message: 'CLIENT REACT STARTED'});
};


const subscribeToDetectingStarted = cb => {
    console.log('[clientSocketIOApi.js] - Subscribed to the [detecting-started] Event!');
    reactSocket.on('detecting-started', (obj) => cb(obj));
}


// DETECTING PERSON IMAGE FASE
const subscribeToPersonHashSent = cb => {
    console.log('[clientSocketIOApi.js] - Subscribed to the [person-hash-sent] Event!');
    reactSocket.on('person-hash-sent', (obj) => cb(obj));
}
 
const subscribeToConfirmPersonRecording = (cb) => {
    console.log('[clientSocketIOApi.js] - Subscribed to the [confirm-person-recording-sent] Event!');
    reactSocket.on('confirm-person-recording-sent', (obj) => cb(obj));
}

const emitConfirmPersonRecordingEvent = (obj) => {
    console.log('[clientSocketIOApi.js] - Sending the [confirm-person-recording-sent] Event!');
    reactSocket.emit('confirm-person-recording-sent', obj);
};


// REGISTER PERSON IMAGE FASE
const subscribeToRegisterImageStartedSent = (cb) => {
    console.log('[clientSocketIOApi.js] - Subscribed to the [register-image-started] Event!');
    reactSocket.on('register-image-started', (obj) => cb(obj));
}
//
const emitRegisterImageStartedEvent = (obj) => {
    console.log('[clientSocketIOApi.js] - Sending the [register-image-started] Event!');
    reactSocket.emit('register-image-started', {personHash: obj.personHash});
};
//
const emitRegisterImageNotStartedEvent = (obj) => {
    console.log('[clientSocketIOApi.js] - Sending the [register-image-not-started] Event!');
    reactSocket.emit('register-image-not-started', {personHash: obj.personHash});
}
//
const emitRegisterImageFinishedEvent = (obj) => {
    console.log('[clientSocketIOApi.js] - Sending the [register-image-finished] Event!');
    reactSocket.emit('register-image-finished', obj);
};
//
const emitRegisterImageCanceled = (obj) => {
    console.log('[clientSocketIOApi.js] - Sending the [register-image-canceled] Event!');
    reactSocket.emit('register-image-canceled', {personHash: obj.personHash});
}

const subscribeToRegisterImageSuccessfulEvent = (cb) => {
    console.log('[clientSocketIOApi.js] - Subscribed to the [register-image-successful] Event!');
    reactSocket.on('register-image-successful', (obj) => cb(obj));
}


//
//MOCK PYHTON CALLS
const pythonSocket = openSocket('http://localhost:5000/face-recog-python-cli');

const mockPyEmitPersonNameSentEvent = (hash) => {
     console.log('[clientSocketIOApi.js] - Sending the  MOCK:[person-hash-sent] Event!');
    pythonSocket.emit('person-hash-sent', {message: 'MOCK: PERSON NAME SENT', personHash: hash});
};
//
const mockPyEmitUnknownPersonEvent = () => {
    console.log('[clientSocketIOApi.js] - Sending the  MOCK:[unknown-person-sent] Event!');
    pythonSocket.emit('unknown-person-sent', 'MOCK: UNKNOWN PERSON SENT');
};

//
export  {
    mockPyEmitPersonNameSentEvent,
    mockPyEmitUnknownPersonEvent,
    subscribeToExpressOnline, 
    subscribeToDetectingStarted,
    subscribeToPersonHashSent,
    subscribeToConfirmPersonRecording,
    emitConfirmPersonRecordingEvent,
    subscribeToRegisterImageStartedSent,
    emitClientStartedEvent,
    emitRegisterImageStartedEvent,
    emitRegisterImageNotStartedEvent,
    emitRegisterImageFinishedEvent,
    emitRegisterImageCanceled,
    subscribeToRegisterImageSuccessfulEvent
 } ;