import React from 'react';
import { 
  Button,
  Form,
  Input,
  Row,
  Col
} from 'reactstrap';

import { 
  Speecher,
  textToSpeech
} from '../Speecher/Speecher' 
 
const  speechMessage = (haveToSpeech, message) => {
  // Speech message!
  if(haveToSpeech) {
     textToSpeech(message);
  }
  //
  return message;
}

const PersonNameForm = (props) => {
    //
    return(
      <div>        
          <p className="h1 text-success">{speechMessage(props.speech,'Digite seu Nome ou se preferir acione o microfone')}</p>
          <Form onSubmit={props.submit}>  
          <Row>
              <Col md={12} >                               
                <Input autoFocus required
                    type="text" 
                    name="postTextToServer" 
                    id="postTextToServer" 
                    placeholder="Corrija aqui caso necessário!"
                    value={props.value}
                    onChange={props.changed} 
                    className="text-success"             
                />  
              </Col>
          </Row> 
          <br/>
          <Row> 
              <Col>
                <Button  className="text-info" type="submit"  disabled={props.buttonDisabled}>Cadastrar</Button>
                <Button  className="text-warning" onClick={props.clickNo} disabled={props.buttonDisabled} >Não</Button>
              </Col>
          </Row>
          <Row>
            <Col md={12}>
                  <Speecher 
                      outputTextId='postTextToServer'
                      elementType='inputText'
                      microphoneClicked={props.microphoneClicked}
                  />             
            </Col>         
          </Row>
        </Form>
     </div>
    );
  }
  
export default PersonNameForm;