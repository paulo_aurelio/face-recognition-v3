import React from 'react'
import { 
    Button
  } from 'reactstrap';
import { textToSpeech } from '../Speecher/Speecher' 

const  speechMessage = (haveToSpeech, message) => {
    // Speech message!
    if(haveToSpeech) {
       textToSpeech(message);
    }
    //
    return message;
}

const DisplayInvitationToRegister = (props) => {
    //
    return (
        <div>
            <p className="h1 text-success">{speechMessage(props.speech, 'Olá Tudo Bem ?')}</p>
            <p className="h1 text-success">{speechMessage(props.speech, 'Cadastre sua imagem aqui!')}</p>
            <Button 
                className="text-success"                
                onClick={props.confirmationHandler} 
            >Cadastro</Button>
        </div> 
    )
};

export default DisplayInvitationToRegister;