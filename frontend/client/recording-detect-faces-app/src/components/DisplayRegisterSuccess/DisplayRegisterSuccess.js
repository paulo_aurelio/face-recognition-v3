import React from 'react'

import { textToSpeech } from '../Speecher/Speecher' 

const  speechMessage = (haveToSpeech, message) => {
    // Speech message!
    if(haveToSpeech) {
       textToSpeech(message);
    }
    //
    return message;
}

const DisplayRegisterSuccess = (props) => {
    //
    return (
        <div>
            <p className="h1 text-success">{speechMessage(props.speech, 'Tudo certo. Obrigado por se cadastrar!')}</p>            
        </div> 
    )
};

export default DisplayRegisterSuccess;