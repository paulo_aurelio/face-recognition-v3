//
import React, {Component} from 'react';
//import micOffLogo from './assets/img/mic.gif';
//import micOnLogo from './assets/img/mic-animate.gif';
//
import micOffLogo from './assets/img/mic.gif';
import micOnLogo from './assets/img/giphy-animated.gif';
//
import { 
    SpeecherApi,
    TextToSpeechApi
} from './SpeechApi';

//
const SpeechToTextClickableImage = (props) => {
    console.log('[SpeechToTextClickableImage] outputTextOn: ' + props.outputTextOn)
    console.log('[SpeechToTextClickableImage] outputTextId: ' + props.outputTextId)
    return (
        <div>
            <img onClick={props.clicked} alt="turn on microphone" 
                 id={props.clickableId}  src={micOffLogo} width='50px'/>
            {props.outputTextOn && <div id={props.outputTextId} className="h1 text-success"/>}
        </div> 
    );                              
}
//
class Speecher extends Component {
    
    state = {
        microphoneOn: false,
        outputSpeechToTextOn: false,
        finalSpeechTranscrition:''
    };

    constructor(props) {
        
        super(props);
   
        //optional parameters.
        this._outputTextOn = false;
        let outputTextId = props.outputTextId;
        if(!outputTextId) { // if (undefined)
            outputTextId = 'output_speech_to_text';
            //
            this._outputTextOn = true;
        }
        this._speechToTextOutputDOMElemtId = outputTextId;

        const elementTypesSupported= ['div','inputText'];
        let elemtType = props.elementType;
        if(!elemtType) { // if (undefined)
            elemtType = 'div';
        } else {
            //'inputText' -- só suporta inputText por enquanto
            if (!(elementTypesSupported.includes(elemtType))){                
                throw Error('DOM Element type ['+ elemtType +'] is not supported');
            }
        }
        this._speechToTextOutputDOMElemttype = elemtType;

        // Web Speech API Instance
        this._speecherRecognitionApiInstance = 
        new SpeecherApi(
            this._speechToTextOutputDOMElemtId, 
            this._speechToTextOutputDOMElemttype
        );
        this._speechToTextClickableElementId = 'start_mic_img';
        //
        // Construcitng the CallBack function to Export Internal state to main component.
        this._microphoneClicked = () => props.microphoneClicked(
            this._speecherRecognitionApiInstance.finalSpeechTranscrition, // property
            this.state.microphoneOn) //component state

    }
        
    //After render() mount the Component into de DOM
    componentDidMount() {
       this.setState({outputSpeechToTextOn: this._outputTextOn});
    }
    
    //click to speech on Microphone!!!
    toogleMicrophoneOnOff = () => {
        console.log('toogleMicrophoneOnOff- id of clickable '+ this._speechToTextClickableElementId)
        if(this.state.microphoneOn){      
            this._speecherRecognitionApiInstance.stopSpeechToText();
            document.getElementById(this._speechToTextClickableElementId).src = micOffLogo;
            // resetMicrophone and get Latst Speech Transcription
            this.setState({
                microphoneOn: false,
                finalSpeechTranscrition: this._speecherRecognitionApiInstance.finalSpeechTranscrition
            });
            //
            //this._speecherRecognitionApiInstance.finalSpeechTranscrition());
            //
        } else {
             this._speecherRecognitionApiInstance.startSpeechToText();
            document.getElementById(this._speechToTextClickableElementId).src = micOnLogo;
            // setMicrophone
            this.setState({
                microphoneOn: true,
                finalSpeechTranscrition:''
            })
        }
    }
    
    render(){
        return (
           <SpeechToTextClickableImage 
               clicked={() => {
                   this.toogleMicrophoneOnOff();
                   this._microphoneClicked();
                }} 
               clickableId={this._speechToTextClickableElementId}               
               outputTextId={this._speechToTextOutputDOMElemtId}
               outputTextOn={this.state.outputSpeechToTextOn}
            />);
    }
}

//
// Text to Speech function
// 
const textToSpeech = (message) => { 

    if (message !== ''){
        const instance = new TextToSpeechApi();
        instance.textToSpeech(message);   
        console.log('[textToSpeech]: message= ' + message);        
    }
};
//
export { 
    Speecher,
    textToSpeech
};
