//
//
class SpeecherApi {

    //Construct Initial State
    _recognizing = false;
    _finalSpeechTranscrition = '';
    _speecherRecognition = null;
    //
    constructor(speechToTextOutputDOMElemtId, speechToTextOutputDOMElemtType) {
        let recognition = null;
        // Check if Browser support Speeching (Web Speech API)
        if ( !('webkitSpeechRecognition' in window) && !('SpeechRecognition' in window) ) {
            console.log('Speech API not supported in this Browser');
        } else { 
            console.log('Speech API IS supported in this Browser!');            
            window.SpeechRecognition = window.webkitSpeechRecognition || window.SpeechRecognition;
            //That is the object that will manage our whole recognition process. 
            recognition = new window.SpeechRecognition();

            //Suitable for dictation.
            recognition.continuous = true;   
            
            //If we want to start receiving results even if they are not final.
            recognition.interimResults = true;  
            
            //Define some more additional parameters for the recognition:
            recognition.lang = 'pt-BR'; // "en-US"; 

            //Since from our experience, the highest result is really the best...
            recognition.maxAlternatives = 1; 
            //recognition.maxAlternatives = 10;
            
            //
            recognition.onstart = () => {
                //Listening (capturing voice from audio input) started.
                //This is a good place to give the user visual feedback about that (i.e. flash a red light, etc.)
                console.log('Recognition: Recognition On Start'); 
                //
                this._recognizing=true;  
            };
            //
            recognition.onend = () => {
                //Again – give the user feedback that you are not listening anymore. If you wish to achieve continuous recognition – you can write a script to start the recognizer again here.
                console.log('Recognition: Recognition On End'); 
                this._recognizing=false;                     
            };
            //
            recognition.onspeechstart = function() {
                console.log('Recognition: Start Speeching has being detected'); 
            };  
            // 
            recognition.onspeechend = () => {                 
                console.log('Recognition: Speeching has stopped being detected'); 
            }
            //
            recognition.onnomatch = function(event) {
                console.log('Recognition: Speeching has No Matching detected'); 
            };
            //
            recognition.onerror = function(event) {
                console.log('Recognition: Error on Speeching has being detected'); 
            };
            //
            recognition.onresult = (event) => { //the event holds the results
                //We have results! Let’s check if they are defined and if final or not:
                if (typeof(event.results) === 'undefined') { //Something is wrong…
                    recognition.stop();
                    return;
                }
                //
                let finalTranscript = '';
                let interimTranscript = '';
                for (var i = event.resultIndex; i < event.results.length; ++i) {
                    var transcript = event.results[i][0].transcript;     
                    if (event.results[i].isFinal) { //Final results
                        //Of course – here is the place to do useful things with the results.
                        console.log("final results: " + transcript);  
                        finalTranscript += transcript;                         
                    } else { //i.e. interim...  
                        //You can use these results to give the user near real time experience.                        
                        console.log("interim results: " + transcript);  
                        interimTranscript += transcript;
                    } 
                } //end for loop
                //
                this._finalSpeechTranscrition = finalTranscript + interimTranscript;
                console.log('[Recognition] finalSpeechTranscrition: ' + this._finalSpeechTranscrition); 
                //                
                if(speechToTextOutputDOMElemtId && speechToTextOutputDOMElemtType){
                    //document.getElementById(_finalSpeechTranscritionDOMElemetId).innerHTML =  '<p class="h1 text-success">' + this._finalSpeechTranscrition + '</p>';
                    if(speechToTextOutputDOMElemtType === 'div'){
                        document.getElementById(speechToTextOutputDOMElemtId).innerHTML = this._finalSpeechTranscrition;
                    } else if(speechToTextOutputDOMElemtType === 'inputText') {
                        document.getElementById(speechToTextOutputDOMElemtId).value = this._finalSpeechTranscrition;
                    } 
                    //
                    console.log('Recognition: Wrote to the DOM: [type= ' + 
                    speechToTextOutputDOMElemtType + ', id= ' + speechToTextOutputDOMElemtId+ '] - finalSpeech: ' 
                    + this._finalSpeechTranscrition);
                } else {
                    const msgError = '[Recognition] speechToTextOutputDOMElemtId and speechToTextOutputDOMElemtType is undefined';
                    console.log(msgError); 
                    throw Error(msgError);
                }               
            }; 
        }
        //setup Speech Web APi for Speech to Text
        this._speecherRecognition = recognition;
    }
  
    // Property Getter method
    get finalSpeechTranscrition() {
        return this._finalSpeechTranscrition;
    }

    // functions Class methods
    startSpeechToText = () => {
      if(!this._recognizing){
        this._speecherRecognition.start(); 
      }  
    }

    stopSpeechToText = () => {
        if(this._recognizing) {
            this._speecherRecognition.stop(); 
        }       
    }

    abortSpeechToText = () => {
        this._speecherRecognition.abort(); 
    }
}
//
class TextToSpeechApi {

    constructor() {
        // Check if Browser support Speeching (Web Speech API)
        if ( !('speechSynthesis' in window) ) {
            console.log('TextToSpeech API not supported in this Browser');   
            this._synth = null;         
        } else { 
            this._synth = window.speechSynthesis;
        }
    }    

    textToSpeech = (message) => {

        if(this._synth !== null) {
            const utterThis = new SpeechSynthesisUtterance(message);
            utterThis.lang = 'pt-BR';
            this._synth.speak(utterThis);  
            console.log('Text-to-Speech: spoke text: ' + message);       
        } else {
            throw Error('Text-to-Speech API was not initialized');       
        }
    }
}


export { 
    SpeecherApi,
    TextToSpeechApi
};




















