import React from 'react'
import { 
   Button
  } from 'reactstrap';
import { textToSpeech } from '../Speecher/Speecher'   

const  speechMessage = (haveToSpeech, message) => {
   // Speech message!
   if(haveToSpeech) {
      textToSpeech(message);
   }
   //
   return message;
}

const ConfirmRegistration = (props) => {   
    return ( 
       <div>
        <p className="h1 text-success">{speechMessage(props.speech, 'Podemos iniciar ?')}</p>
        <Button className="text-info"    onClick={props.clickYes}>Sim</Button>
        <Button className="text-warning" onClick={props.clickNo}>Não</Button>
       </div>
    );
   }

export default ConfirmRegistration;