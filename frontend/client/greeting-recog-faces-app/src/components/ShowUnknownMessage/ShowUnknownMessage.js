import React from 'react'
import { textToSpeech } from '../Speecher/Speecher' 

const  speechMessage = (haveToSpeech, message) => {
    // Speech message!
    if(haveToSpeech) {
       textToSpeech(message);
    }
    //
    return message;
}

const ShowUnknownMessage = (props) => {   
    return ( 
       <div>
        <p className="h1 text-success">{speechMessage(props.speech, 'Olá, ainda não te conheço!')}</p>                  
       </div>
    );
   }

export default ShowUnknownMessage;