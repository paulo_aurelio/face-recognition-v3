import React from 'react'
  
 // FUNCTION COMPOMENTS
 const ShowServerState = (props) => {      
    let styleClasses="h5 text-info fixed-top";
    let flag = "ON";
    if (!props.state) {
        styleClasses="h5 text-danger fixed-top";
        flag="OFF";
    } 
    return <p className={styleClasses}>Greeting Recog Faces v1.00 - Server {flag}</p>
  }

export default ShowServerState;