import React from 'react'
import { textToSpeech } from '../Speecher/Speecher' 

const   greetingPerson = (name) => {
    //
    let greeting = "";

    const d = new Date();
    const time = d.getHours();
    //
    greeting = "Bom dia ";
    if (time >= 12 && time < 18) {
    greeting = "Boa tarde ";
    } else if(time >= 18) {
    greeting = "Boa noite ";
    }
    //
    greeting = greeting.concat(name)
    // Speech Greeting!
    textToSpeech(greeting);
    //
    return greeting;
  }

const DisplayGreeting = (props) => {
    //
    if (props.personName === ''){
        return null;
    }
    //
    return <p className="h1 text-success">{greetingPerson(props.personName)}</p>;
};

export default DisplayGreeting;