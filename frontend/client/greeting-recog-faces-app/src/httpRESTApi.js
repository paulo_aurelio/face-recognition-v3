
  
  
  //doing a get!
  const callGETApi = async (url) => {
    const response = await fetch(url);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

// sample Call get
    /*
    callGETApi('/api/hello')
      .then(res => this.setState({ response: res.express }))
      .catch(err => console.log(err));
    */

  const callPOSTApi =  async (url, data) => {
    //
    const response = await fetch(url, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({ post: data }),
    });
    //
    const body = await response.text();

    return body;
  }
   // Sample
  //const body = await callPOSTApi('/api/world',  this.state.post);    
  //this.setState({ responseToPost: body });

  export {callGETApi , callPOSTApi};