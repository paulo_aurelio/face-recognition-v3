import openSocket from 'socket.io-client';


const reactSocket = openSocket('http://localhost:5000/face-recog-react-cli');


// INITIALIZATOIN FASE
const subscribeToExpressOnline = cb => {
    reactSocket.on('expressOnline', (bol) => cb(bol));
}

const emitClientStartedEvent = () => {
    reactSocket.emit('client-started', {message: 'CLIENT REACT STARTED'});
};

const subscribeToDetectingStarted = cb => {
    reactSocket.on('detecting-started', (obj) => cb(obj));
}


// DETECTING PERSON IMAGE FASE
const subscribeToPersonNameSent = cb => {
    reactSocket.on('person-name-sent', (obj) => cb(obj));
}
 
const subscribeToUnknownPersonSent = (cb) => {
    reactSocket.on('unknown-person-sent', (obj) => cb(obj));
}

// REGISTER PERSON IMAGE FASE

const emitRegisterImageStartedEvent = () => {
    reactSocket.emit('register-image-started', {message: 'REGISTER IMAGE STARTED Event'});
};

const subscribeToRegisterImageStartedSent = (cb) => {
    reactSocket.on('register-image-started', (obj) => cb(obj));
}

const emitPersonNameSentEvent = (obj) => {
    console.log('emitPersonNameSentEvent- obj: ' + JSON.stringify(obj));
    reactSocket.emit('person-name-sent', {
        message: 'PERSON NAME SENT EVENT', 
        personName: obj.personName, 
        personHash: obj.personHash 
    });
};
//
//
//MOCK PYHTON CALLS
const pythonSocket = openSocket('http://localhost:5000/face-recog-python-cli');

const mockPyEmitPersonNameSentEvent = (name) => {
    pythonSocket.emit('person-name-sent', {message: 'MOCK: PERSON NAME SENT', personName: name});
};
//
const mockPyEmitUnknownPersonEvent = () => {
    pythonSocket.emit('unknown-person-sent', 'MOCK: UNKNOWN PERSON SENT');
};

//
export  {
    mockPyEmitPersonNameSentEvent,
    mockPyEmitUnknownPersonEvent,
    subscribeToExpressOnline, 
    subscribeToDetectingStarted,
    subscribeToPersonNameSent,
    subscribeToUnknownPersonSent,
    emitClientStartedEvent,
    emitRegisterImageStartedEvent,
    subscribeToRegisterImageStartedSent,
    emitPersonNameSentEvent
 } ;