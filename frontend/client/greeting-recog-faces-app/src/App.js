import React, { Component } from 'react';

import { 
  Button,
  Row,
  Card,
  CardBody
} from 'reactstrap';

import {  
  mockPyEmitPersonNameSentEvent, 
  mockPyEmitUnknownPersonEvent,
  subscribeToExpressOnline,
  subscribeToDetectingStarted, 
  subscribeToPersonNameSent,
  subscribeToUnknownPersonSent,
  emitClientStartedEvent
 } from './clientSocketIOApi';

// 
// React Compoments
//
import ShowServerState from './components/ShowServerState/ShowServerState' 
//
//App Components
//import ShowUnknownMessage from './components/ShowUnknownMessage/ShowUnknownMessage'
import DisplayGreeting from './components/DisplayGreeting/DisplayGreeting'

import './App.css';
 
// CLASS COMPONENTS
class App extends Component {

  constructor(props) {
    super(props);
 
    //INITIALIZATION FASE
    subscribeToExpressOnline( bol => this.setState({
        serverExpressOnLine: bol
    }));
    
    subscribeToDetectingStarted(obj => this.setState({ //Initial App State
        operationMode: obj.message,
        personName:'',
        personHash:''
    }));

    //DETECTING PERSON IMAGE FASE
    subscribeToPersonNameSent(obj => 

        this.setState({
          operationMode: obj.message,
          personName: obj.personName,
          personHash: ''
        }, () => setTimeout(() => emitClientStartedEvent(),3000))

    );

    subscribeToUnknownPersonSent(obj => {
      console.log('subscribeToUnknownPersonSent- obj: ' + JSON.stringify(obj));
      this.setState({
        operationMode: obj.message,
        personName: '',
        personHash: obj.personHash
      });
    
    });  
  }
  // App State Object
  state = { // Initial State
    operationMode: 'Initialised: Waiting for Detecting Person Image to Start',
    testerPyButtonsVisible: false, 
    serverExpressOnLine : false,
    personName: '',
    personHash: '',
  };
  //
  /*
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  */
  //
  //
  componentDidMount() { 
    //
    /*
    this.timerID = setInterval(
      () => { 
         // Initial screen!
         if( this.state.operationMode === 'Unknown Person Sent' || this.state.operationMode === 'Person Name Sent') {
            emitClientStartedEvent();
         }         
      },
      10000 // 20 secods in first screen idle!!!
    );
    */
    // Initialization Fase Finished, sending client started event!
    emitClientStartedEvent();
  }
  //
  //
  // render React Compoments
  //
  render() {
    //
    // Main component redering
    //
    return (
      <div className="App fixed-bottom">
        <Row>
            <ShowServerState state={this.state.serverExpressOnLine} />      
        </Row>
        <Card className="Card-App justify-content-center align-items-center">
          <CardBody>
            { 
              //    'Initialised: Waiting for Detecting Person Image to Start' and 
              //    'Detecting Person Image Started'
              (this.state.operationMode === 'Person Name Sent') && 
                  <DisplayGreeting 
                      personName={this.state.personName}
                  />
            }
            {
              // Unknown Person Sent
              /*
              (this.state.operationMode === 'Unknown Person Sent') && 
                  <ShowUnknownMessage 
                      speech={true}
                    />
                    */
            }
          </CardBody>
        </Card>
        <p className="text-info">{this.state.operationMode} </p>               
        {this.state.testerPyButtonsVisible && <Button size='sm' onClick={() => mockPyEmitPersonNameSentEvent("Tester")} >Send Name</Button> }
        {this.state.testerPyButtonsVisible && <Button size='sm' onClick={() => mockPyEmitUnknownPersonEvent()} > Unknown Person</Button>   }                 
      </div>
    );
  }
}
//
export default App;
