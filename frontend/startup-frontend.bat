@echo off

SETLOCAL 
: instalation dependencies
set PROJ_HOME=c:\lab\proj\python-lab\face-recognition-v3

: setting basics
set THIS_BATCH_SHELL_HOME=%PROJ_HOME%\frontend
IF [%1] EQU [%THIS_BATCH_SHELL_HOME%] (
    set CURRENT_DIR=%1
    set STAND_ALONE=FALSE
) ELSE (
    set CURRENT_DIR=%CD%
    set STAND_ALONE=TRUE
)
IF %CURRENT_DIR% NEQ %THIS_BATCH_SHELL_HOME% GOTO THIS_SHELL_LOCATION
cd %THIS_BATCH_SHELL_HOME%

: preparing variable
set RECORDING_FRONTEND_HOME=%PROJ_HOME%\frontend\client\recording-detect-faces-app
set GREETING_FRONTEND_HOME=%PROJ_HOME%\frontend\client\greeting-recog-faces-app

: echo PROJ_HOME.......................: %PROJ_HOME%
: echo THIS_BATCH_SHELL_HOME...........: %THIS_BATCH_SHELL_HOME%
: echo CURRENT_DIR.....................: %CURRENT_DIR%
: echo RECORDING_FRONTEND_HOME.........: %RECORDING_FRONTEND_HOME%
: echo GREETING_FRONTEND_HOME..........: %GREETING_FRONTEND_HOME%

: MAIN_SHELL
echo Checking Installation of Frontend preconditions...
IF NOT EXIST %THIS_BATCH_SHELL_HOME%\node_modules (
    echo Missing Dependencies!
    echo Installing Environment Dependencies, Please Wait!
    call npm install
    echo done!
)
echo Client React Application checkings...
cd %RECORDING_FRONTEND_HOME%
IF NOT EXIST %RECORDING_FRONTEND_HOME%\node_modules (
    echo Missing Dependencies!
    echo Installing ReactJs Recording Application Dependencies, Please Wait!
    call npm install
    echo ReactJs Recording Application Installed!
)

cd %GREETING_FRONTEND_HOME%
IF NOT EXIST %GREETING_FRONTEND_HOME%\node_modules (
    echo Missing Dependencies!
    echo Installing ReactJs Greeting Application Dependencies, Please Wait!
    call npm install
    echo Greeting Application Installed!
)

echo Starting Frontend ReactJs Applications... 
cd %THIS_BATCH_SHELL_HOME%
IF %STAND_ALONE% EQU TRUE (
    call npm run dev
) ELSE (
    start npm run dev
) 
GOTO END

:THIS_SHELL_LOCATION
echo This shell is supposed to be in %THIS_BATCH_SHELL_HOME%
GOTO END

:END
IF %STAND_ALONE% EQU TRUE (
    cd %CURRENT_DIR%
) 
