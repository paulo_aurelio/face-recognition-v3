@echo off
SETLOCAL

: instalation dependencies
set PROJ_HOME=c:\lab\proj\python-lab\face-recognition-v3

: setting basics
set THIS_BATCH_SHELL_HOME=%PROJ_HOME%\backend\apps\greeting-faces\bin
IF [%1] EQU [%THIS_BATCH_SHELL_HOME%] (
    set CURRENT_DIR=%1
    set STAND_ALONE=FALSE
) ELSE (
    set CURRENT_DIR=%CD%
    set STAND_ALONE=TRUE
)
IF %CURRENT_DIR% NEQ %THIS_BATCH_SHELL_HOME% GOTO THIS_SHELL_LOCATION

: preparing variable
set PYTHON_GREETING_HOME=%PROJ_HOME%\backend\apps\greeting-faces
set DATA_HOME=%PROJ_HOME%\backend\data
set PICKLE_PATH=%DATA_HOME%\pickle

: Parameters for pyhton shell
set PICKLE_FILENAME=%PICKLE_PATH%\encodings.pickle

: echo PROJ_HOME......................: %PROJ_HOME%
: echo THIS_BATCH_SHELL_HOME..........: %THIS_BATCH_SHELL_HOME%
: echo CURRENT_DIR....................: %CURRENT_DIR%
: echo PYTHON_GREETING_HOME..........: %PYTHON_GREETING_HOME%
: echo DATA_HOME......................: %DATA_HOME%
: echo PICKLE_PATH....................: %PICKLE_PATH%
: echo PICKLE_FILENAME................: %PICKLE_FILENAME%

IF NOT EXIST %PICKLE_FILENAME% ( 
    GOTO PICKLE_DOESNOT_EXIST_ERROR   
) ELSE (
    GOTO MAIN_SHELL
) 

:MAIN_SHELL
echo Starting Python Greeting Faces Application
IF %STAND_ALONE% EQU TRUE (
    python %PYTHON_GREETING_HOME%\greeting-recog-faces.py --encodings %PICKLE_FILENAME%  --display 1 --detection-method hog
) ELSE (
    start python %PYTHON_GREETING_HOME%\greeting-recog-faces.py --encodings %PICKLE_FILENAME%  --display 1 --detection-method hog
) 

GOTO END

:PICKLE_DOESNOT_EXIST_ERROR
echo Pickle file does not exist. Provide this file:
echo %PICKLE_FILENAME%
GOTO END

:THIS_SHELL_LOCATION
echo This shell is supposed to be in %THIS_BATCH_SHELL_HOME%
GOTO END

:END
IF %STAND_ALONE% EQU TRUE (
    cd %CURRENT_DIR%
) 
