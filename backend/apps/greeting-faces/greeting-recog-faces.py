#!/usr/bin/env python
# coding: utf-8

#
# autores:   
#   Andre Mederios
#   Bruno Araujo 
#   Paulo Aurelio Da Silva
#
# USAGE
#
# python recognize_faces_video.py --encodings D:\Project\LiClipseWorkspace\face-recognition-opencv\encodings_test.pickle  --display 0 --detection-method cnn
#
# Refactoring: Paulo Aurélio Da Silva
import threading
import websocket
import json
import os
import unidecode
# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
from imutils import paths
#from os import listdir
#from os.path import isfile, join
#
import face_recognition
import argparse
import imutils
import pickle
import time
import cv2
#import urllib

# parse input arguments
ap = argparse.ArgumentParser()
ap.add_argument("-e", "--encodings", required=True,
	help="path to serialized db of facial encodings")
ap.add_argument("-o", "--output", type=str,
	help="path to output video")
ap.add_argument("-y", "--display", type=int, default=1,
	help="whether or not to display output frame to screen")
ap.add_argument("-d", "--detection-method", type=str, default="hog",
	help="face detection model to use: either `hog` or `cnn`")
args = vars(ap.parse_args())

# Control Pickle File Reload process
mustReloadPickleFile = False

# Main Video Thread Loop
def greetingFacesMain(ws):
    # just declaring this function will use global variable.
	global mustReloadPickleFile
	# load the known faces and embeddings
	print("[INFO] loading encodings...")
	data = pickle.loads(open(args["encodings"], "rb").read())

	# initialize the video stream and pointer to output video file, then
	# allow the camera sensor to warm up
	print("[INFO] starting video stream...")
	fps = FPS().start()
	# Webcan = 0, USBcan = 1
	vs = VideoStream(src=1).start()
	#writer = None
	time.sleep(2.0)

	#
	# loop over frames from the video file stream
	exitMainLoop = False
	name = ''
	find_recog = 0
	curr_name = ''
	#iterations = 0
	#
	# Main Loop
	while not exitMainLoop:

		# Must Reload the known faces and embeddings from file path at --encodings parameter
		if mustReloadPickleFile == True:
			# load the known faces and embeddings
			print("[INFO] reloading encodings...")
			data = pickle.loads(open(args["encodings"], "rb").read()) 
			mustReloadPickleFile = False
		# enf of if
        #
		# print(f"mustReloadPickleFile: {mustReloadPickleFile}")
		#
		# grab the frame from the threaded video stream
		frame = vs.read()
		
		# convert the input frame from BGR to RGB then resize it to have
		# a width of 750px (to speedup processing)
		rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		rgb = imutils.resize(frame, width=450)
		
		r = frame.shape[1] / float(rgb.shape[1])
		
		# detect the (x, y)-coordinates of the bounding boxes
		# corresponding to each face in the input frame, then compute
		# the facial embeddings for each face
		boxes = face_recognition.face_locations(rgb, model=args["detection_method"])
		encodings = face_recognition.face_encodings(rgb, boxes,1)
		names = []

		size = len(encodings)

		# Zero faces
		if size == 0:
			name = ''
			find_recog = 0
			curr_name = ''
			#iterations = 0
		# end of if

		# Find faces
		if size > 0:	
				
			# loop over the facial embeddings
			for encoding in encodings:
				# attempt to match each face in the input image to our known
				# encodings		
		#		face_distances = face_recognition.face_distance(data["encodings"],
		#			encoding)
				
		#		for i, face_distance in enumerate(face_distances):
		#			print("The test image has a distance of {:.2} from known image #{}".format(face_distance, i))
		#			print("- With a normal cutoff of 0.6, would the test image match the known image? {}".format(face_distance < 0.6))
		#			print("- With a very strict cutoff of 0.5, would the test image match the known image? {}".format(face_distance < 0.5))
		#			print()
				
				matches = face_recognition.compare_faces(data["encodings"], encoding, 0.5)
				name = "Unknown"
			
				# check to see if we have found a match
				if True in matches:
					# find the indexes of all matched faces then initialize a
					# dictionary to count the total number of times each face
					# was matched
					matchedIdxs = [i for (i, b) in enumerate(matches) if b]
					counts = {}

					# loop over the matched indexes and maintain a count for
					# each recognized face face
					for i in matchedIdxs:
						name = data["names"][i]
						counts[name] = counts.get(name, 0) + 1
                    # end of for

					# determine the recognized face with the largest number
					# of votes (note: in the event of an unlikely tie Python
					# will select first entry in the dictionary)
					name = max(counts, key=counts.get)
				# end of if

				# update the list of names
				names.append(name)
            #end of for
			#
			# loop over the recognized faces
			for ((top, right, bottom, left), name) in zip(boxes, names):
				# rescale the face coordinates
				top = int(top * r)
				right = int(right * r)
				bottom = int(bottom * r)
				left = int(left * r)

				if name != "Unknown":
					# draw the predicted face name on the image
					cv2.rectangle(frame, (left, top), (right, bottom),(0, 255, 0), 2)
					y = top - 15 if top - 15 > 15 else top + 15
					# unicode(name) for removing accentiation
					cv2.putText(frame, unidecode.unidecode(name) , (left, y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
					
					sizeface = (right - left) * (bottom - top)

					print(sizeface) #//maior que 10000 para cadastro
					
					if sizeface > 2900:
					
						#iterations += 1
						
						#if iterations > 2:
							
						if find_recog == 0:

							if name != curr_name:
								# print(f'Sending name - current name: {name} => last name: {curr_name}')
								# Send name express API (Greetins UI)
								try:
									#urllib.request.urlopen('http://localhost:5000/api/person/name/' + urllib.parse.quote(name)).read()
									ws.send('{"personEvent":"person-name-sent", "personName":"' + name + '"}')
									#print(f"send name: {name}")
								except NameError as error:
									print(f"NameError: Error {error}")
								except Exception as ex:
									print(f"Exception: {ex}")
								find_recog = 1
							# end of if
							curr_name = name
							# end of if
						# end of if
						# end of if	
					else:
						#print('out range')
						name = ''
						find_recog = 0
						#iterations = 0	
					# end else 
				# end if Unknown				
			# if the video writer is None *AND* we are supposed to write
			# the output video to disk initialize the writer
			#if writer is None and args["output"] is not None:
			#	fourcc = cv2.VideoWriter_fourcc(*"MJPG")
			#	writer = cv2.VideoWriter(args["output"], fourcc, 20,
			#		(frame.shape[1], frame.shape[0]), True)

			# if the writer is not None, write the frame with recognized
			# faces to disk
			#if writer is not None:
			#	writer.write(frame)

			# end of for
		# end of if
		# check to see if we are supposed to display the output frame to
		# the screen
		if args["display"] > 0:
			cv2.imshow("Frame", frame)
			
			key = cv2.waitKey(1) & 0xFF

			# if the `q` key was pressed, break from the loop
			if key == ord("q"):
				exitMainLoop = True         
				print(f"{threading.currentThread().getName()} terminating...")
				ws.close()
			# end of if
		# end of if	
		fps.update()
	# end of while not exitMainLoop:	
	#
	fps.stop()
	print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
	print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

	# do a bit of cleanup
	cv2.destroyAllWindows()
	vs.stop()

	# check to see if the video writer point needs to be released
	#if writer is not None:
	#	writer.release()			
# end of greetingFacesMain()

###################
# Websocket Methods
def on_message(ws, message):
	global mustReloadPickleFile
	print(f"Message From Server: {message}")
	#starting Thread to Process new fotos in the on dataset/ folder
	#personEvent:'register-image-completed'
	if 'mustReloadPickleFile' in message:
		mustReloadPickleFile = True
	# end of if
# end of on_message

def on_error(ws, error):
    print(error)
# end of on_error

def on_close(ws):
    print("### closed ###")
# end of on_close

def on_open(ws):
	#run greetingFacesMain in a thread
	t1 = threading.Thread( target=greetingFacesMain, name="Thread-1-greetingFacesMain", args=( ws, ) )
	print (f"Starting {t1.name}")
	t1.start()
	ws.send("[greeting-recog-faces.py]: Threads Started")
# end of on_open

if __name__ == "__main__":
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp("ws://localhost:8765",
                              on_open = on_open,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close).run_forever()
