
@echo off
SETLOCAL

: instalation dependencies
set PROJ_HOME=c:\lab\proj\python-lab\face-recognition-v3

: setting basics
set THIS_BATCH_SHELL_HOME=%PROJ_HOME%\backend\apps\recording-faces\bin
IF [%1] EQU [%THIS_BATCH_SHELL_HOME%] (
    set CURRENT_DIR=%1
    set STAND_ALONE=FALSE
) ELSE (
    set CURRENT_DIR=%CD%
    set STAND_ALONE=TRUE
)
IF %CURRENT_DIR% NEQ %THIS_BATCH_SHELL_HOME% GOTO THIS_SHELL_LOCATION

: preparing variable
set PYTHON_RECORDING_HOME=%PROJ_HOME%\backend\apps\recording-faces
set PYTHON_RECORDING_CONFIG_HOME=%PYTHON_RECORDING_HOME%\config
set DATA_HOME=%PROJ_HOME%\backend\data
set PICKLE_PATH=%DATA_HOME%\pickle

: Parameters for pyhton shell
:     the "dataset" folder name have to be "\" ending it!!!
set PHOTOS_PATH=%DATA_HOME%\dataset\
set PICKLE_FILENAME=%PICKLE_PATH%\encodings.pickle
set CASCADE_FILENAME=%PYTHON_RECORDING_CONFIG_HOME%\haarcascade_frontalface_default.xml

: echo PROJ_HOME......................: %PROJ_HOME%
: echo THIS_BATCH_SHELL_HOME..........: %THIS_BATCH_SHELL_HOME%
: echo CURRENT_DIR....................: %CURRENT_DIR%
: echo PYTHON_RECORDING_HOME..........: %PYTHON_RECORDING_HOME%
: echo PYTHON_RECORDING_CONFIG_HOME...: %PYTHON_RECORDING_CONFIG_HOME%
: echo DATA_HOME......................: %DATA_HOME%
: echo PICKLE_PATH....................: %PICKLE_PATH%
: echo PHOTOS_PATH....................: %PHOTOS_PATH%
: echo PICKLE_FILENAME................: %PICKLE_FILENAME%
: echo CASCADE_FILENAME...............: %CASCADE_FILENAME%

IF NOT EXIST %PICKLE_FILENAME% ( 
    GOTO PICKLE_DOESNOT_EXIST_ERROR   
) ELSE (
    GOTO MAIN_SHELL
) 

:MAIN_SHELL
echo Starting Python Recording Faces Application
IF %STAND_ALONE% EQU TRUE (
    python %PYTHON_RECORDING_HOME%\recording-recog-faces.py --cascade %CASCADE_FILENAME% --photos %PHOTOS_PATH%  --encodings %PICKLE_FILENAME% --detection-method hog
) ELSE (
    start python %PYTHON_RECORDING_HOME%\recording-recog-faces.py --cascade %CASCADE_FILENAME% --photos %PHOTOS_PATH%  --encodings %PICKLE_FILENAME% --detection-method hog
) 

GOTO END

:PICKLE_DOESNOT_EXIST_ERROR
echo Pickle file does not exist. Provide this file:
echo %PICKLE_FILENAME%
GOTO END

:THIS_SHELL_LOCATION
echo This shell is supposed to be in %THIS_BATCH_SHELL_HOME%
GOTO END

:END
IF %STAND_ALONE% EQU TRUE (
    cd %CURRENT_DIR%
) 
