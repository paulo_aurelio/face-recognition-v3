#!/usr/bin/env python
# coding: utf-8 

# autores:   
#   Andre Mederios
#   Bruno Araujo 
#   Paulo Aurelio Da Silva
#
############################
# Refactoting: Paulo Aurelio
import threading
import websocket
import json
import os
#import urllib
import pickle
import face_recognition
###############################
# import the necessary packages
from scipy.spatial import distance as dist
# from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils.video import FPS
from imutils import face_utils
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2
import shutil


# parse input arguments
ap = argparse.ArgumentParser()

ap.add_argument("-c", "--cascade", type=str, required=True, help="path to cascade configuation")
ap.add_argument("-p", "--photos", type=str, required=True, help="path to photos folder")
ap.add_argument("-e", "--encodings",  type=str, required=True, help="path to serialized db of facial encodings")
ap.add_argument("-d", "--detection-method", type=str, required=True, help="face detection model to use: either `hog` or `cnn`")

args = vars(ap.parse_args())


# File Path
# DATA_HOME = '../../../data/'
#PHOTOS_PATH = DATA_HOME + 'dataset/'
#print (args["photos"])
PHOTOS_PATH = args["photos"]
PHOTOS_TAKE_PICTURES_FILENAME = 'takePicture-hash'
#CASCADE_CONFIG_PATH = '../config/'
#CASCADE_FILENAME = CASCADE_CONFIG_PATH + 'haarcascade_frontalface_default.xml'
#print(args["cascade"])
CASCADE_FILENAME = args["cascade"]

#PICKLE_PATH = DATA_HOME + 'pickle/'
#PICKLE_FILE_NAME = PICKLE_PATH + 'encodings.pickle'

#print (args["encodings"])
PICKLE_FILE_NAME = args["encodings"]

print (f"Detection Method: {args['detection_method']}")
DETECTION_METHOD = args["detection_method"]

#control flow variables
takePictures = False

# Main Video Thread Loop
def recordingFacesMain(ws):
    global takePictures
    # print(f"PHOTOS_PATH={PHOTOS_PATH}")
    # print(f"CASCADE_FILENAME={CASCADE_CONFIG_PATH + CASCADE_FILENAME}")

    # initialize dlib's face detector (HOG-based) and then create
    # the facial landmark predictor
    print("[INFO] loading facial landmark predictor...")
    faceCascade = cv2.CascadeClassifier(CASCADE_FILENAME)

    # start the video stream thread
    print("[INFO] starting video stream thread...")
    fps = FPS().start()
    # Webcan = 0
    vs = VideoStream(src=0).start()

    print("INICIADO...")
    time.sleep(2.0)
    # time_out_to_erase = 0
    emptyframe = True
    i_cdtp = 0
    # loop over frames from the video stream
    # Detecting if we have Only One face in the video at the desired distance
    hashx = int(time.time())
    exitMainLoop = False
    while not exitMainLoop:

        # Read detecting faces
        frame = vs.read()
        # frame = cv2.flip(frame, -1)
        frame = imutils.resize(frame, width=530)
        cv2.rectangle(frame, (20, 20), (20, 20), (255, 0, 0), 2)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30)
        )

        # print('Read detecting faces')

        countFaces = len(faces)

        # Zero faces
        if countFaces == 0:
            emptyframe = True
            # print('Zero faces')
            hashx = int(time.time())
        # Find faces
        if emptyframe and countFaces == 1:
            
            for (x, y, w, h) in faces:

                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

                sizeface = w*h

                #print(f"size face: {sizeface}")
                #print(f"hash: {hashx}")

                if sizeface > 13000:

                    emptyframe = False

                    # print(f'Find face near: {sizeface}')

                    # Create hash dir
                    try:
                        #path_w_photos = PHOTOS_PATH + str(hashx) + '-hash'
                        #check_exist_dir_timestamp = os.path.isdir(path_w_photos)

                        #if not check_exist_dir_timestamp:
                        #    os.mkdir(path_w_photos)
                        # end of if
                        # Send hash express API
                        hashToSend = str(hashx)
                        # urllib.request.urlopen('http://localhost:5000/api/person/hash/' + hashToSend).read()
                        ws.send('{"personEvent":"person-hash-sent", "personHash":"' + hashToSend + '"}')
                    except NameError as error:
                        print(f"NameError: Error {error}")
                    except Exception as ex:
                        print(f"Exception: {ex}")  
                    #end of try
                # end of if
            #end of for
        # end of if
        #
        # Take photos
        if takePictures:
            #Take picutres!!!    
            while i_cdtp <= 30:
                if i_cdtp in (0, 5, 10, 15, 20, 25):

                    path_tp = PHOTOS_PATH + PHOTOS_TAKE_PICTURES_FILENAME

                    save_photo = str(hashx + i_cdtp)
                    p = os.path.sep.join([path_tp, "{}.png".format(save_photo)])
                    cv2.imwrite(p, frame)
                # end of if
                i_cdtp += 1
            # end of while    
            takePictures = False
        else:
            i_cdtp = 0
        # end if else    
        key = cv2.waitKey(1) & 0xFF
		#if the `q` key was pressed, break from the loop
        if key == ord("q"):            
            exitMainLoop = True         
            print(f"{threading.currentThread().getName()} terminating...")
            ws.close()
		# end of if
        cv2.imshow("Frame", frame)

        fps.update()
    # end of While
    fps.stop()
    print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

    vs.stop()
# end of recordingFacesMain()

# Create Folder for Take Picture Method.
def create_picture_folder(hash):
    # just declaring this function will use global variable.
    global takePictures
    print (f"[recording-recog-faces.py] - Starting {threading.currentThread().getName()}")
    # Check if Folder was Successful created (Renamed from 9999999-hash)
    try:
        # Check if TP Folder Exist! If So, Must be deleted
        check_dir_tp = os.path.isdir(PHOTOS_PATH + PHOTOS_TAKE_PICTURES_FILENAME)
        if check_dir_tp:
            #Erase previous TP folder
            erase_picture()
        #end of if
        os.mkdir(PHOTOS_PATH + PHOTOS_TAKE_PICTURES_FILENAME)
        print(f"{PHOTOS_PATH + PHOTOS_TAKE_PICTURES_FILENAME} was created.")
        #set Reload Flg.    
        takePictures = True		
    except NameError as error:
        print(f"NameError: Error {error}")
        print(f"{PHOTOS_PATH + PHOTOS_TAKE_PICTURES_FILENAME} was NOT created.")
    except Exception as ex:
        print(f"Exception: {ex}")  
        print(f"{PHOTOS_PATH + PHOTOS_TAKE_PICTURES_FILENAME} was NOT created.")
    # end of try
    time.sleep(1)
    print(f"[{threading.currentThread().getName()}] - Terminating...")
# end of create_picture_folder

def erase_picture():
    #
    print (f"[recording-recog-faces.py] - Starting {threading.currentThread().getName()}")
    # Erase folder
    try:
        path = PHOTOS_PATH
        filelist1 = [f for f in os.listdir(path) if f.endswith("-hash")]
        print(f"file list to delete: {filelist1}")
        for f in filelist1:
            pathToDelete = os.path.join(path, f)
            print(f"PATH to Delete: {pathToDelete}")
            shutil.rmtree(pathToDelete, ignore_errors=True)
        # end of for
    except NameError as error:
        print(f"NameError: Error {error}")
    except Exception as ex:
        print(f"Exception: {ex}")
    time.sleep(1)
    print(f"[{threading.currentThread().getName()}] - Terminating...")
# end of erase_pictures

########################
# Pickle File Update
#
def process_pickle(ws, name, hash):
	#
	print(f"[{threading.currentThread().getName()}] - Starting {PHOTOS_PATH} file process: personName={name} personHash={hash}")
	#
	checkExist_PHOTOS_PATH = os.path.isdir(PHOTOS_PATH)
	#
	if checkExist_PHOTOS_PATH:
	#
		if name != '':
			name.strip()
			name = name.capitalize()
			name = name.split(' ', 1)[0]
		# end of if
		folder = PHOTOS_PATH  + PHOTOS_TAKE_PICTURES_FILENAME

		# Call encode new faces into pickle
		encode_face(folder, name)

		# Rename folder to NAME-[hash]
		new_folder = PHOTOS_PATH   + name + '-' + hash
		os.rename(folder, new_folder)
        #
		ws.send('mustReloadPickleFile')
		time.sleep(1)
	#end of if PHOTOS_PATH exist
	else:
		print(f"[{threading.currentThread().getName()}] - PHOTOS_PATH: {PHOTOS_PATH} does not exist!!!")
	# end else
	print(f"[{threading.currentThread().getName()}] - Terminating...")
# end of process_pickle


# Process Pickle File
def encode_face(dataset, person_name):
	imagePaths = [f for f in os.listdir(dataset) if os.path.isfile(os.path.join(dataset, f))]

	# initialize the list of known encodings and known names
	knownEncodings = []
	knownNames = []

	print("Dataset")
	print(dataset)
	print("ImagePaths List")
	print(imagePaths)

	# loop over the image paths
	for imagePath in imagePaths:

		print("Image Path")
		print(imagePath)

		name = person_name

		photo_load = dataset + '/' + imagePath
		print("Photo Load")
		print(photo_load)

		image = cv2.imread(photo_load)
		rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

		# detect the (x, y)-coordinates of the bounding boxes
		# corresponding to each face in the input image
		boxes = face_recognition.face_locations(rgb, model=DETECTION_METHOD)

		# compute the facial embedding for the face0
		encodings = face_recognition.face_encodings(rgb, boxes)

		# loop over the encodings
		for encoding in encodings:
			# add each encoding + name to our set of known names and
			# encodings
			knownEncodings.append(encoding)
			knownNames.append(name)
		# end of for
    # end of for
	# dump the facial encodings + names to disk
	print("[INFO] serializing encodings...")

	# Read Pickle Array
	data = pickle.loads(open( PICKLE_FILE_NAME , "rb").read())

	# Append Pickle Array
	data["names"] += knownNames
	data["encodings"] += knownEncodings

	print(data)

	f = open(PICKLE_FILE_NAME , "wb")
	print('pickle append')
	f.write(pickle.dumps(data))
	f.close()
# end of encode_face

###################
# Websocket Methods
def on_message(ws, message):
	print(f"Message From Server: {message}")
	# Event to Process
    #
	#personEvent:'register-image-accepted'
	if 'register-image-accepted' in message:
		obj = json.loads(message)
		print (f"[recording-recog-faces.py] - personEvent= {obj['personEvent']} hash= {obj['personHash']}")
		t2 = threading.Thread( target=create_picture_folder, name="Thread-2-Create_Picture_Folder", args=(obj['personHash'],) )
		t2.start()
	# end of if
    #
	#personEvent:'register-image-canceled'
	if 'register-image-canceled' in message:
		obj = json.loads(message)
		print (f"[recording-recog-faces.py] - personEvent= {obj['personEvent']} hash= {obj['personHash']}")
		t3 = threading.Thread( target=erase_picture, name="Thread-3-Erase_Pictures", args=() )
		t3.start()
    # end of if
    #
	#personEvent:'register-image-completed'
	if 'register-image-completed' in message:
		obj = json.loads(message)
		print (f"[recording-recog-faces.py] - personEvent= {obj['personEvent']} personName= {obj['personName']} hash= {obj['personHash']}")
		t4 = threading.Thread( target=process_pickle, name="Thread-4-Process-Pickle", args=(ws, obj['personName'], obj['personHash']) )
		t4.start()
	# end of if
# end of on_message

def on_error(ws, error):
    print(error)
# end of on_error

def on_close(ws):
    print("### closed ###")
# end of on_close

def on_open(ws):
	#run recordingFacesMain in a thread
	t1 = threading.Thread( target=recordingFacesMain, name="Thread-1-recordingFacesMain", args=( ws, ) )
	print (f"Starting {t1.name}")
	t1.start()
	ws.send("[recording-recog-faces]: Threads Started")
# end of on_open

if __name__ == "__main__":
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp("ws://localhost:8765",
                              on_open = on_open,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close).run_forever()
# end of if