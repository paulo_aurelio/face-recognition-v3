
//----------- SERVER ----------

const WebSocket = require('ws'); 
const ws = new WebSocket('ws://localhost:8765');

ws.on('open', () =>  {
  console.log("[server-frontend.js] - Ws Channel openned!!!");
    ws.send('Server Frontend - CONNECTED!!!');
});

ws.on('message', data => { 
  console.log("[server-frontend.js] - Received from server-broadcast - WS Channel: " + data);

  if (data.includes('person-name-sent')) {

    if (reactFaceRecog) {
      let obj = JSON.parse(data);
      let name = obj.personName;
      //
      try {
        name = utf8.decode(name);      
      } catch (error) {
        console.error(`Error: ${error}`)
      }
      //
      //pythonSocketClient.emit('person-name-sent', {personName: name});
      reactFaceRecog.emit('person-name-sent', { 
        message: 'Person Name Sent',
        personName: name
      });
      //
    } else {
      console.error("reactFaceRecog IN NOT DEFINED");
    }

  } else if (data.includes('person-hash-sent')){
    
    if (reactFaceRecog) {
      let obj = JSON.parse(data);
      let hash = obj.personHash;
      //
      try {
        hash = utf8.decode(hash);      
      } catch (error) {
        console.error(`Error: ${error}`)
      }
      //
      //pythonSocketClient.emit('person-hash-sent', {personHash: hash});
      reactFaceRecog.emit('person-hash-sent', { 
        message: 'Person Hash Sent',
        personHash: hash
      });
      //
    } else {
      console.error("reactFaceRecog IN NOT DEFINED");
    }
  }


});
// Send message to Ws Channel where python client are listining!!!
const sendObjToBroadcast = (obj) => {
    console.log('[server-frontend.js] - Sending obj to [server-broadcast.js]');
    ws.send(obj);
};

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const utf8 = require('utf8');
let pythonSocketClient = null;


// HTTP REST API METHODS
app.get('/api', (req, res) => {
  console.log('[server-frontend.js] - REST [/api] - ' + req);
  sendObjToBroadcast(JSON.stringify({ name: 'paulo', hash:'test-xxxx-123'}));
  res.send({ express: 'Server On line!!!' });
});

/*
app.post('/api/person', (req, res) => {

  console.log(req.body);

  res.sendStatus(200); // equivalent to res.status(200).send('OK')
  
  res.send(
    `${req.body.post}`,
  );
  
});
*/

//
// FASE 1 - DETECTION and RECORDING
//
/*
app.get('/api/person/hash/:hash', (req, res) => {
  console.log('[server-frontend.js] - REST GET [/api/person/:hash] - ' + req);
  //
  if (pythonSocketClient) {
    //
    let hash = req.params.hash;
    hash = utf8.decode(hash);
    //
    pythonSocketClient.emit('person-hash-sent', {personHash: hash});
    //
    res.sendStatus(200);
  } else {
    res.sendStatus(409);
  }
});
*/
//
// FASE 2 - RECOGNITION
///
/*
app.get('/api/person/name/:name', (req, res) => {
  console.log('[server-frontend.js] - REST GET [/api/person/name/:name] - ' + req);
  //
  if (pythonSocketClient) {
    let name = req.params.name;
    //
    try {
      name = utf8.decode(name);      
    } catch (error) {
      console.error(`Error: ${error}`)
    }
    //
    pythonSocketClient.emit('person-name-sent', {personName: name});
    //
    res.sendStatus(200);
  } else {
    res.sendStatus(409);
  }

});
*/
//
/*
app.get('/api/person/unknown', (req, res) => {
  console.log('[server-frontend.js] - REST GET [/api/person/unknown] - ' + req);
  //
  if (pythonSocketClient) {
    //
    pythonSocketClient.emit('unknown-person-sent', {});
    //
    res.sendStatus(200);
  } else {
    res.sendStatus(409);
  }
  //
});
*/
//
// Not using now,because we are going to define a Socke.io Serer!
// app.listen(port, () => console.log(`Listening on port ${port}`));
//
//
// Server Socket.io
//
const server = require('http').Server(app);
const io = require('socket.io')(server);

// SOCKET.IO API CALLS
const pythonFaceRecog = io.of('/face-recog-python-cli');

// PYTHON APP CONNECTION TO CHANNEL
pythonFaceRecog.on('connection', pySocket => {
    //
    // INITIALIZATOIN FASE
    console.log('[server-frontend.js] - pythonClient has CONNECTED!') 
    pySocket.emit('expressOnline', true);

    pySocket.on('client-started', event => {
        console.log('[server-frontend.js] - pythonClient is ready!', event);    
    });
    //
    //
    // FASE 1
    // DETECTING PERSON IMAGE FASE
    //
    //
    //PERSON-HASH-SENT
    /*
    pySocket.on('person-hash-sent', event => {
        console.log('[server-frontend.js] - Received [person-hash-sent] Event from pythonClient!: ', event); 
        //
        reactFaceRecog.emit('person-hash-sent', { 
            message: 'Person Hash Sent',
            personHash: event.personHash
        });
        //
        console.log('[server-frontend.js] - Sending to reactClient: [person-hash-sent] Event');  
    });
    */
    //
    // FASE 2
    // RECOGNING PERSON IMAGE FASE
    //PERSON-NAME-SENT
    /*
    pySocket.on('person-name-sent', event => {
      console.log('[server-frontend.js] - Received [person-name-sent] Event from pythonClient!: ', event); 
      //
      reactFaceRecog.emit('person-name-sent', { 
          message: 'Person Name Sent',
          personName: event.personName
      });
      //
      console.log('[server-frontend.js] - Sending to reactClient: [person-name-sent] Event'); 
    });
    */
    /*
    //UNKOWM PERSON SENT
    pySocket.on('unknown-person-sent', event => {
      console.log('[server-frontend.js] - Received [person-name-sent] Event from pythonClient!: ', event); 
      //
      reactFaceRecog.emit('unknown-person-sent', { 
            message: 'Unknown Person Sent', 
            personHash: event.personHash
      });
      //
      console.log('[server-frontend.js] - Sending to reactClient: [unknown-person-sent] Event'); 
    });
    */
    //  
    //
    //on Disconnect 
    pySocket.on('disconnect', () => {
      console.log('[server-frontend.js] - pythonClient has DISCONNECTED!');      
    });

});

const reactFaceRecog = io.of('/face-recog-react-cli');

//
// REACT APP CONNECTION TO CHANNEL
//
reactFaceRecog.on('connection', reactSocket => {  
    //
    // INITIALIZATOIN FASE
    console.log('[server-frontend.js] - reactClient has CONNECTED!') 
    reactSocket.emit('expressOnline', true);
    //
    // CLIENT-STARTED 
    // DETECTING FACES FASE
    reactSocket.on('client-started', event => {
      //
      console.log('[server-frontend.js] - reactClient is ready!', event);
      
      reactSocket.emit('detecting-started',{ message: 'Detecting Person Image Started'});
      console.log('[server-frontend.js] - Sending to reactClient: [detecting-started] Event');    
    });
    //
    // CONFIRM IMAGE RECORDING  
    reactSocket.on('confirm-person-recording-sent', event => {
      //
      console.log('[server-frontend.js] - Received [confirm-person-recording-sent] Event from reactClient!: ', event); 

      reactSocket.emit('confirm-person-recording-sent', { message: 'Confirm Person Recording'});
      console.log('[server-frontend.js] - Sending to reactClient: [confirm-person-recording-sent] Event');   
    });
    //
    // REGISTER-IMAGE-STARTED
    reactSocket.on('register-image-started', event => {
      //
      console.log('[server-frontend.js] - Received [register-image-started] Event from reactClient!: ', event); 

      reactSocket.emit('register-image-started', { 
          message: 'Register Person Image Started',
          personHash: event.personHash
      });
      console.log('[server-frontend.js] - Sending to reactClient: [register-image-started] Event');   
      
      /*
      pythonFaceRecog.emit('take-picture-person-sent', {
          personEvent:'register-image-accepted',
          personHash: event.personHash
      });
      */
      const person = JSON.stringify({
          personEvent:'register-image-accepted',
          personHash: event.personHash
      }); 
      console.log(`[server-frontend.js] - [reactSocket] sendObjToBroadcast(${person})`);
      sendObjToBroadcast(person);

      console.log('[server-frontend.js] - Sending to WS pyhtonClient: [register-image-accepted] Event');
    });
    //
    // REGISTER-IMAGE-NOT-STARTED
    reactSocket.on('register-image-not-started', event => {
      //
      console.log('[server-frontend.js] - Received [register-image-not-started] Event from reactClient!: ', event); 

      // Reinicia a Ui do React para a Tela Inicial [Detecting Person Image Started]
      console.log('[server-frontend.js] - Sending to reactClient: [detecting-started] Event');
      reactSocket.emit('detecting-started',{ message: 'Detecting Person Image Started'});
      // 
    });    
    //
    // REGISTER-IMAGE-FINISHED
    reactSocket.on('register-image-finished', event => {
      console.log('[server-frontend.js] - Received [register-image-finished] Event from reactClient!', event); 
      //
      //Sending personName sent by Ui React
      /*
      pythonFaceRecog.emit('register-image-finished', {
          personEvent:'register-image-completed',        
          personName: event.personName,
          personHash: event.personHash
      });
      */
      console.log('[server-frontend.js] - Sending to WS pyhtonClient: [register-image-completed] Event!'); 
      const person = JSON.stringify( {
        personEvent:'register-image-completed',        
        personName: event.personName,
        personHash: event.personHash
      }); 
      console.log(`[server-frontend.js] - [reactSocket] sendObjToBroadcast(${person})`);
      sendObjToBroadcast(person);

      console.log('[server-frontend.js] - Sent [register-image-completed] Event to pyhtonClient!'); 
      //
      // Reinicia a Ui do React para a Tela Inicial [Detecting Person Image Started]
      reactSocket.emit('register-image-successful',{ message: 'Register Person Image Successful'});
      console.log('[server-frontend.js] - Sending to reactClient: [register-image-successful] Event');       
    });
    //
    // REGISTER-IMAGE-CANCEL
    reactSocket.on('register-image-canceled', event => {
      console.log('[server-frontend.js] - Received [register-image-canceled] Event from reactClient!', event); 
      //
      //Sending personName sent by Ui React
      console.log('[server-frontend.js] - Sending to WS pyhtonClient: [register-image-canceled] Event!'); 
      /*
      pythonFaceRecog.emit('erase-picture-person-sent', {
        personEvent:'register-image-canceled',
        personHash: event.personHash
      });
      */
      //
      const person = JSON.stringify({
        personEvent:'register-image-canceled',
        personHash: event.personHash
      });
      //
      console.log(`[server-frontend.js] - [reactSocket] sendObjToBroadcast(${person})`);
      sendObjToBroadcast(person);
      //
      // Reinicia a Ui do React para a Tela Inicial [Detecting Person Image Started]
      console.log('[server-frontend.js] - Sending to reactClient: [detecting-started] Event');
      reactSocket.emit('detecting-started',{ message: 'Detecting Person Image Started'});
      //       
    });
    //
    //
    //on Disconnect 
    reactSocket.on('disconnect', () => {
      console.log('[server-frontend.js] - reactClient has DISCONNECTED!'); 
    });  
});
//
//
server.listen(port, () => { 

  console.log(`[server-frontend.js] - Server is Listening on port ${port}`)
  console.log(`[server-frontend.js] - Starting Socket.io-client Proxy for Phyton App`)

  //doing a get!
  const fetch = require('node-fetch');
  const callGETApi = async (url) => {
      //
      console.log('[server-frontend.js] - callGETApi- GET: [' + url +']');  
      let encodedUrl = utf8.encode(url);    
      const response = await fetch(encodedUrl);
      const body = await response.json();
      //
      console.log('[server-frontend.js] - callGETApi- response: [' + url +'] data: ' + JSON.stringify(body));
      if (response.status !== 200) {
        console.log('response.status: ' + response.status +'Error msg: ' + body.message)
        throw Error(body.message);
      } 
  };
  
  //OPEN SOCKET.IO FOR PYTHON COMUNICATION (THROUGH API)
  const openSocketIOClient = require('socket.io-client');
  pythonSocketClient = openSocketIOClient('http://localhost:5000/face-recog-python-cli');
  pythonSocketClient.on('connect', function (socket) {
    console.log('[server-frontend.js] - [pythonSocketClient] Client Connected!');
  }); 


  // REACT SENT 'register-image-finished' EVENT for PYTHON RECOG APP
  pythonSocketClient.on('register-image-finished', function (data) {
     // console.log('[server-frontend.js] - [pythonSocketClient] Client was [register-image-finished] Event: ' + JSON.stringify(data));
    //
    //let url = 'http://localhost:5001/api/person/name';
    //url = url +'/' + data.personName+'/'+data.personHash;
    //
    //console.log('[server-frontend.js] - calling callGETApi...: url: [' + url +'] data: ' + JSON.stringify(data));
    //
    // send  to python REST
    //callGETApi(url)
    //.then(res => { 
        //console.log('[server-frontend.js] - [pythonSocketClient] callGETApi RESPONSE: ' + res);
        /*
        const person = JSON.stringify(data); 
        console.log(`[server-frontend.js] - [pythonSocketClient] sendObjToBroadcast(${person})`);
        sendObjToBroadcast(person);
        */
     //})
    //.catch(err => console.log('[server-frontend.js] - [pythonSocketClient] callGETApi ERROR: ' + err));
    //
  });

  // REACT SENT 'take-picture-person-sent' EVENT for PYTHON RECOG APP
  pythonSocketClient.on('take-picture-person-sent', function (data) {
    // console.log('[server-frontend.js] - [pythonSocketClient] Client was sent [take-picture-person-sent] Event: ' + JSON.stringify(data));
    //
    //let url = 'http://localhost:5001/api/person/takePicture';
    //url = url + '/' + data.personHash;
    //
    //console.log('[server-frontend.js] - calling callGETApi...: url: [' + url +'] data: ' + JSON.stringify(data));
    //
    // send  to python REST
    //callGETApi(url)
    //.then(res => { 
        //console.log('[server-frontend.js] - [pythonSocketClient] callGETApi RESPONSE: ' + res);
        /*
        const person = JSON.stringify(data); 
        console.log(`[server-frontend.js] - [pythonSocketClient] sendObjToBroadcast(${person})`);
        sendObjToBroadcast(person);
        */
    //})
    //.catch(err => console.log('[server-frontend.js] - [pythonSocketClient] callGETApi ERROR: ' + err));
    //
  });

  // REACT SENT 'erase-picture-person-sent' EVENT for PYTHON RECOG APP
  pythonSocketClient.on('erase-picture-person-sent', function (data) {
    // console.log('[server-frontend.js] - [pythonSocketClient] Client was sent [erase-picture-person-sent] Event: ' + JSON.stringify(data));
    //
    //let url = 'http://localhost:5001/api/person/erasePicture';
    //url = url + '/' + data.personHash;
    //
    //console.log('[server-frontend.js] - calling callGETApi...: url: [' + url +'] data: ' + JSON.stringify(data));
    //
    // send  to python REST
    //callGETApi(url)
    //.then(res => { 
      //console.log('[server-frontend.js] - [pythonSocketClient] callGETApi RESPONSE: ' + res);
      /*
      const person = JSON.stringify(data);
      console.log(`[server-frontend.js] - [pythonSocketClient] sendObjToBroadcast(${person})`);
      sendObjToBroadcast(person);
      */
    //})
    //.catch(err => console.log('[server-frontend.js] - [pythonSocketClient] callGETApi ERROR: ' + err));
    //
  });
  // - end
});
