@echo off

SETLOCAL 
: instalation dependencies
set PROJ_HOME=c:\lab\proj\python-lab\face-recognition-v3

: setting basics
set THIS_BATCH_SHELL_HOME=%PROJ_HOME%\backend
IF [%1] EQU [%THIS_BATCH_SHELL_HOME%] (
    set CURRENT_DIR=%1
    set STAND_ALONE=FALSE
) ELSE (
    set CURRENT_DIR=%CD%
    set STAND_ALONE=TRUE
)
IF %CURRENT_DIR% NEQ %THIS_BATCH_SHELL_HOME% GOTO THIS_SHELL_LOCATION
cd %THIS_BATCH_SHELL_HOME%

: echo PROJ_HOME..........................: %PROJ_HOME%
: echo THIS_BATCH_SHELL_HOME..............: %THIS_BATCH_SHELL_HOME%
: echo CURRENT_DIR........................: %CURRENT_DIR%

: MAIN_SHELL
echo Checking preconditions...
IF NOT EXIST %THIS_BATCH_SHELL_HOME%\node_modules (
    echo Installing Backend Dependencies
    call npm install
)
echo Starting WebSocket Servers
IF %STAND_ALONE% EQU TRUE (
    call npm run dev
) ELSE (
    start npm run dev
) 

GOTO END

:THIS_SHELL_LOCATION
echo This shell is supposed to be in %THIS_BATCH_SHELL_HOME%
GOTO END

:END
IF %STAND_ALONE% EQU TRUE (
    cd %CURRENT_DIR%
) 
