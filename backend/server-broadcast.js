/*
#
# autor: Paulo Aurelio Da Silva
#
*/

const WebSocket = require('ws');
 
console.log('[server-broadcast.js] - Listening at 8765');
const server = new WebSocket.Server({ port: 8765 });


server.on('connection', socket => {

  socket.on('message', data => {
    console.log(`[server-broadcast.js] - received from a client: ${data}`);
    // Broadcast to everyone.
    server.clients.forEach(client => {
      if (/*client !== socket && */ client.readyState === WebSocket.OPEN) {
        client.send(data);
      }
    });
  });
});